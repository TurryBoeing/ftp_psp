/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ftp_servidor;

import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Servidor
 */
public class Datos extends Thread {

    Scanner indatos = new Scanner(System.in);
    private final int PUERTODATOS = 2047;
    String queArchivo = "";
    private ServerSocket conexionDatos;
    private DataOutputStream salida;
    private InputStream lecturaFichero;
    private DataInputStream datosLecturaFichero;

    public Datos(String archivo) {
        this.queArchivo = archivo;
    }

    @Override
    public void run() {
        try {
            conexionDatos = new ServerSocket(PUERTODATOS);
            System.out.println("Conexion de datos de FTP Lanzada y esuchando en puerto " + PUERTODATOS);
            Socket cliente = conexionDatos.accept();
            salida = new DataOutputStream(cliente.getOutputStream());
            while (cliente.isConnected()) {
                if (cliente.isClosed()) {
                    System.out.println("Cerrando conexion de Datos");
                    cliente.close();
                    conexionDatos.close();
                    break;
                }
                switch (queArchivo) {
                    case "Invierno":
                        lecturaFichero = new FileInputStream("C:\\Documents and Settings\\Servidor\\Mis documentos\\NetBeansProjects\\FTP_Servidor\\src\\ficherosAnonimo\\Invierno.jpg");
                        //lecturaFichero = new FileInputStream("C://Prueba.txt");
                        datosLecturaFichero = new DataInputStream(lecturaFichero);
                        int leidoi = 0;
                        while ((leidoi = datosLecturaFichero.read()) != -1) {
                            salida.write(leidoi);
                            System.out.println(datosLecturaFichero.available());
                            if (datosLecturaFichero.available() == 0) {
                                salida.close();
                                lecturaFichero.close();
                                datosLecturaFichero.close();
                                break;
                            }
                        }
                        queArchivo = "";
                        break;
                    case "Prueba":
                        lecturaFichero = new FileInputStream("C://Prueba.txt");
                        datosLecturaFichero = new DataInputStream(lecturaFichero);
                        int leidop = 0;
                        while ((leidop = datosLecturaFichero.read()) != -1) {
                            salida.write(leidop);
                            if (datosLecturaFichero.available() == 0) {
                                salida.close();
                                lecturaFichero.close();
                                datosLecturaFichero.close();
                                break;
                            }
                        }
                        queArchivo = "";
                        break;
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            System.exit(0);
        }
    }
}
