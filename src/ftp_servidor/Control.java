/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ftp_servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Servidor
 */
public class Control extends Thread {

    private Datos conexionDatos;
    private final int PUERTOCONTROL = 2021;
    private ServerSocket conexionControl;

    @Override
    public void run() {
        try {
            conexionControl = new ServerSocket(PUERTOCONTROL);
            File archivos = new File("C:\\Documents and Settings\\Servidor\\Mis documentos\\NetBeansProjects\\FTP_Servidor\\src\\ficherosAnonimo");
            String[] listaArchivos = archivos.list();
            System.out.println("Conexion de control de FTP Lanzada y esuchando en puerto " + PUERTOCONTROL);
            Socket cliente = conexionControl.accept();
            while (cliente.isConnected()) {
                if (cliente.isClosed()) {
                    break;
                }
                DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
                //salida.writeUTF("Hola cliente: Soy un prototipo de FTP. Te enviaré un archivo de los que tengo en mi lista. \n Tú debes elegir uno. \n 1: (IMG) Invierno \n 2:(TXT) Prueba");
                salida.writeUTF("Hola cliente: Soy un prototipo de FTP. Te enviare un archivo de los que tengo en mi lista. \n Tu debes elegir uno.");
                for (int i = 0; i < listaArchivos.length; i++) {
                    salida.writeUTF((i + 1) + " - " + listaArchivos[i]);
                }
                salida.writeUTF("Puedes hablar... ");
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                int opcionElegida = Integer.valueOf(entrada.readUTF());
                System.out.println("Cliente dice: " + opcionElegida);
                switch (opcionElegida) {
                    case 1:
                        salida.writeUTF("Has elegido que te envie la imagen invierno. \n Escribe S si es correcto, y en cuanto lo hagas iniciare la transferencia");
                        salida.writeUTF("Puedes hablar... ");
                        if ("S".equals(entrada.readUTF())) {
                            salida.writeUTF("Enviando el tamaño del archivo");
                            File tamanhoFoto = new File("C:\\Documents and Settings\\Servidor\\Mis documentos\\NetBeansProjects\\FTP_Servidor\\src\\ficherosAnonimo\\Invierno.jpg");
                            salida.writeUTF(String.valueOf(tamanhoFoto.length()));
                            Thread.sleep(500);
                            salida.writeUTF("Iniciando la transferencia de Invierno... ");
                            conexionDatos = new Datos("Invierno");
                            conexionDatos.start();
                        }
                        break;
                    case 2:
                        salida.writeUTF("Has elegido que te envie el documento Prueba. \n Escribe S si es correcto, y en cuanto lo hagas iniciaré la transferencia");
                        salida.writeUTF("Puedes hablar... ");
                        if ("S".equals(entrada.readUTF())) {
                            salida.writeUTF("Enviando el tamaño del archivo");
                            File tamanhoPrueba=new File("C:\\Documents and Settings\\Servidor\\Mis documentos\\NetBeansProjects\\FTP_Servidor\\src\\ficherosAnonimo\\Prueba.txt");
                            salida.writeUTF(String.valueOf(tamanhoPrueba.length()));
                            Thread.sleep(500);                            
                            salida.writeUTF("Iniciando la transferencia de Prueba... ");
                            conexionDatos = new Datos("Prueba");
                            conexionDatos.start();                            
                        }
                        break;
                }
            }
            conexionControl.close();
        } catch (IOException ex) {
            System.out.println("Error I/O " + ex.getMessage());
            try {
                conexionControl.close();
            } catch (IOException ex1) {
                System.out.println("Imposible cerrar la conexion: " + ex1.getMessage());
            }
        }catch(InterruptedException ex){
            System.out.println("Proceso interrumpido");
        }
    }
}
